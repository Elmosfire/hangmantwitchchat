from pathlib import Path
import re
import urllib.request
from tqdm import tqdm
from random import choice, shuffle
from collections import Counter, deque
import threading
from time import time


def initialise_word_list():
    if not Path("wordlist.txt").exists():
        with urllib.request.urlopen(
            "https://gist.githubusercontent.com/deekayen/4148741/raw/98d35708fa344717d8eee15d11987de6c8e26d7d/1-1000.txt"
        ) as gfile:
            with Path("wordlist.txt").open("w") as file:
                for x in tqdm(gfile):
                    line = x.decode("utf-8").strip().lower()
                    if re.match("^[a-z]{5}[a-z]*$", line):
                        file.write(line + "\n")
    with Path("wordlist.txt").open("r") as file:
        return list(map(str.strip, file))

class DataExpander:
    def __init__(self):
        self.data = {}
        
    def register_data(self, data, columns):
        for col, dat in zip(columns, zip(*data)):
            self.data[col] = list(dat)
        
    def expand_data_for_ajax_output(self, sz):
        res = {}
        for k,v in self.data.items():
            for i in range(sz):
                if len(v) > i:
                    res[f"{k}_{i}"] = v[i]
                else:
                    res[f"{k}_{i}"] = ""
        return res
    
            
    

class Hangman:
    words = initialise_word_list()
    max_lives = 3

    def __init__(self):
        self.previous_word = "n/a"
        self.current_word = "n/a"
        self.locktime = 0
        self.guesses = {}
        self.randomise_word()
        self.score = Counter()
        self.history = deque()

    def randomise_word(self):
        self.locktime = time() + 5
        self.previous_word = self.current_word
        self.current_word = choice(self.words)
        self.guesses = {}

    def iter_guesses(self):
        for x in self.guesses.values():
            yield from x

    def do_guess(self, playername, letter):
        if time() < self.locktime:
            return
        if letter in self.iter_guesses():
            return
        if self.lives(playername) <= 0:
            return
        
        if letter in self.current_word:
            self.history.appendleft((playername, letter + " \u2713"))
        else:
            self.history.appendleft((playername, letter + " \u2a2f"))
        if len(self.history) > 10:
            self.history.pop()
        
        
        if playername not in self.guesses:
            self.guesses[playername] = []
        self.guesses[playername].append(letter)

        if playername not in self.score:
            self.score[playername] = 0
        if letter in self.current_word:
            self.score[playername] += 1

        if not (set(self.current_word) - set(self.iter_guesses())):
            self.randomise_word()

        

    def lives(self, playername):
        return self.max_lives - sum(
            1 for x in self.guesses.get(playername, []) if x not in self.current_word
        )
        
    def livesstr(self, playername):
        return "\u2665" * self.lives(playername)

    def statistics(self):
        for x in sorted(self.score, key=self.score.get, reverse=True):
            yield x, "".join(self.guesses.get(x, [""])), self.livesstr(x), self.score[x]

    def expanded_statistics(self):
        res = {"title": self.current_letters(), "prev": self.previous_word}
        if time() < self.locktime:
            res = {"title": self.previous_word, "prev": self.previous_word}
        res["guessed"] = "".join(x for x in sorted(self.iter_guesses()) if x not in self.current_word)
        
        d = DataExpander()
        d.register_data(self.statistics(), ["player_name", "player_guesses", "player_lives", "player_score"])
        d.register_data(self.history, ["hist_name", "hist_guess"])
        d.register_data([[x] for x in self.score if self.lives(x) <= 0], ["grave_name"])
        res.update(d.expand_data_for_ajax_output(6))
        
        return res

    def register_twitch_conn(self, conn):
        def loc(self, conn):
            with conn as conn2:
                for x in conn2.loop():
                    if (
                        len(x.message) == 1
                        and x.message.lower() in "abcdefghijklmnopqrstuvwxyz"
                    ):
                        self.do_guess(x.user, x.message.lower())

        threading.Thread(None, loc, args=(self, conn)).start()

    def current_letters(self):
        s = set(self.iter_guesses())
        return " ".join(x if x in s else "_" for x in self.current_word)
