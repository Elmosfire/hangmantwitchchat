import argparse
from flask import Flask

argparser = argparse.ArgumentParser(description="Elefense migration CLI")
argparser.add_argument("option", help="Options: <runserver>")
args = argparser.parse_args()


def serve():
    import entrypoint


if args.option == "runserver":
    serve()
