from flask import Flask, render_template
import os
from hangman import Hangman
from twitch_interface import TwitchMockInterface, TwitchInterface


app = Flask(__name__)

hangman = Hangman()
hangman.register_twitch_conn(TwitchMockInterface(hangman))
hangman.register_twitch_conn(TwitchInterface("#elmusfire"))


@app.route("/")
def home():
    return render_template("index.html")


@app.route("/tmp.json")
def tmp():
    return hangman.expanded_statistics()


port = int(os.environ.get("PORT", 5000))
app.run(debug=True, host="0.0.0.0", port=port)
