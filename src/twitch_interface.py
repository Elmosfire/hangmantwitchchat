from re import X
import socket
from emoji import demojize
import json
from types import SimpleNamespace
import yaml
from random import choice, shuffle, random
from time import sleep


class TwitchInterface:
    try:
        settings = SimpleNamespace(
            **yaml.load(open("/app/settings.yml"), yaml.SafeLoader)
        )
    except FileNotFoundError:
        settings = SimpleNamespace(**yaml.load(open("settings.yml"), yaml.SafeLoader))

    def __init__(self, channel):
        self.channel = channel

    def __enter__(self):
        self.sock = socket.socket()

        self.sock.connect((self.settings.server, self.settings.port))
        self.sock.send(f"PASS {self.settings.token}\n".encode("utf-8"))
        self.sock.send(f"NICK {self.settings.nickname}\n".encode("utf-8"))
        self.sock.send(f"JOIN {self.channel}\n".encode("utf-8"))
        self.sock.send(
            f"PRIVMSG {self.channel} : {self.settings.nickname} is active\n".encode(
                "utf-8"
            )
        )

        return self

    def __exit__(self, *_):
        self.sock.close()

    def loop(self):
        while True:
            resp = self.sock.recv(2048).decode("utf-8")

            if resp.startswith("PING"):
                self.sock.send("PONG\n".encode("utf-8"))

            elif len(resp) > 0:
                print(resp)
                for line in resp.split("\r\n"):
                    message_full = str(demojize(line))
                    if message_full.count(":") >= 2:
                        message = message_full.split(":", 2)[-1].strip()
                    if message == "Unknown command\r\n":
                        continue
                    try:
                        user = message_full.split(":", 2)[1].split("!")[0]
                    except:
                        print("invalid: ", repr(message))
                        continue
                    print("message:", repr(message))
                    # if len(message) < 4:
                    #    self.send_message(f"got message {message} from user {user}")
                    yield SimpleNamespace(user=user, message=message)

    def send_message(self, message):
        print(f"PRIVMSG {self.channel} :{message}\n")
        self.sock.send(f"PRIVMSG {self.channel} :{message}\n".encode("utf-8"))


class TwitchMockInterface(TwitchInterface):
    delay = 5

    def __init__(self, hangman_parent):
        self.channel = ""
        self.hangman_parent = hangman_parent

    def __enter__(self):
        return self

    def __exit__(self, *_):
        pass

    def loop(self):
        playernames = [f"botPlayer75{x}" for x in range(10)]
        while True:
            insert = list("abcdefghijklmnopqrstuvwxyz")
            shuffle(insert)
            for x in insert:
                if x in self.hangman_parent.iter_guesses():
                    continue
                #if x not in self.hangman_parent.current_word:
                #    continue
                sleep(self.delay)
                yield SimpleNamespace(
                    user=choice(playernames),
                    message=x,
                )

    def send_message(self, message):
        print(message)


if __name__ == "__main__":
    with TwitchInterface("#elmusfire") as t:
        for m in t.loop():
            t.send_message("read message " + m.message)
