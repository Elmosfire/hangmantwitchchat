# Flask & Postgresql Docker Template

Docker template for flask api projects and postgresql

## Configuration
Copy `settings_template.yml` to `settings.yml` and enter the correct values.
"server" and "post" should be correct out of the box.
For "nickname" and "token", you create a new twitch account, and past it's name and auth token.

To use the tool in obs, create a browser source with url `http:\\localhost:5000` and set as sizes width 1066 and height 600 (other sizes might work, but are not tested)

## Running (docker)

To run the service, make sure docker is installed and run:

```bash
sudo chmod +x ./service.sh
sudo ./service.sh run
```

If your system does not support bash, or you prefer not to install it, building the docker like this

```bash
docker build -t twitch_hangman .
docker run -d -p 5000:5000 twitch_hangman
```

or equivalent for your system should not work.

## Running (no docker)

To run the system without docker, make sure that python is isntalled.

Next, perform the folling commands

```
pip install -r requirements.txt
python3 src/manage.py runserver
```

Or equivalent for your system.

## Custom wordlists

Comming soon



## License
[MIT](https://choosealicense.com/licenses/mit/)
