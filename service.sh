#!/usr/bin/env bash

if [ "$1" == "build" ]
then
    docker build -t twitch_hangman .
fi

if [ "$1" == "run" ]
then
    docker build -t twitch_hangman .
    docker container stop $(docker container ls -q --filter name=twitch_hangman) || echo "no docker running"
    docker container rm $(docker container ls -qa --filter name=twitch_hangman) || echo "no docker existing"
    docker run --name twitch_hangman -d -p 5000:5000 twitch_hangman 
fi

